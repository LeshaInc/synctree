#[allow(unused_macros)]
macro_rules! sub_tree {
    ($arena:expr, $parent:expr) => ();
    ($arena:expr, $parent:expr,) => ();

    ($arena:expr, $parent:expr, , $($token:tt)*) => (
        sub_tree!($arena, $parent, $($token)*)
    );

    ($arena:expr, $parent:expr, $key:expr) => ({
        let node = $crate::Node::new($key, $arena);
        $parent.append(&node, $arena);
    });

    ($arena:expr, $parent:expr, $key:expr, $($token:tt)*) => ({
        let node = $crate::Node::new($key, $arena);
        $parent.append(&node, $arena);

        sub_tree!($arena, $parent, $($token)*);
    });

    ($arena:expr, $parent:expr, $key:expr => [$($value:tt)*] $($token:tt)*) => ({
        let node = $crate::Node::new($key, $arena);
        $parent.append(&node.clone(), $arena);

        sub_tree!($arena, node, $($value)*);

        sub_tree!($arena, $parent, $($token)*);
    });
}

/// Creates a tree.
///
/// # Examples
///
/// ```ignore
/// let root = tree!(
///     arena,
///     1 => [
///         2,
///         3 => [4, 5],
///         6 => [
///             7 => [8 => []],
///             9 => [10 => [11]]],
///         12
///     ]
/// );
/// ```
///
/// ```ignore
/// let root = tree!(arena, 1);
/// ```
#[macro_export]
macro_rules! tree {
  ($arena:expr, $root_node:expr) => ({
    $crate::Node::new($root_node, $arena)
  });

  ($arena:expr, $root_node:expr => []) => ({
    tree!($arena, $root_node)
  });

  ($arena:expr, $key:expr => [$($value:tt)+]) => ({
    let parent = $crate::Node::new($key, $arena);

    sub_tree!($arena, parent, $($value)+);

    parent
  })
}

#[cfg(test)]
mod tests {
    use ::node_arena::NodeArena;

    #[test]
    fn tree_macro() {
        let ref arena = NodeArena::new();

        let root = tree!(
            arena,
            1 => [
                2,
                3 => [4, 5],
                6 => [
                    7 => [8 => []],
                    9 => [10 => [11]]],
                12
            ]
        );

        let elements = root
            .depth_first_traverse(arena)
            .map(|v| *v.read())
            .collect::<Vec<_>>();
        let expected = vec![1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
        assert_eq!(elements, expected);

        let root = tree!(arena, 1 => [,]);
        let elements = root
            .depth_first_traverse(arena)
            .map(|v| *v.read())
            .collect::<Vec<_>>();
        let expected = vec![1];
        assert_eq!(elements, expected);
    }
}
