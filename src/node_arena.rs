use std::sync::Arc;
use std::ops::{Deref, DerefMut};

use parking_lot::{RwLock, RwLockReadGuard, RwLockWriteGuard};

use arena::Arena;
use super::NodeEntry;

/// A reference-counted [Arena](::arena::Arena) of tree nodes.
///
/// You are free to clone it as much as you want.
#[derive(Debug)]
pub struct NodeArena<T> {
    inner: Arc<RwLock<Arena<NodeEntry<T>>>>,
}

impl<T> NodeArena<T> {
    /// Creates a new, empty `NodeArena`.
    pub fn new() -> NodeArena<T> {
        NodeArena {
            inner: Arc::new(RwLock::new(Arena::new())),
        }
    }

    /// Creates a new `NodeArena` with specified capacity.
    pub fn with_capacity(capacity: usize) -> NodeArena<T> {
        NodeArena {
            inner: Arc::new(RwLock::new(Arena::with_capacity(capacity))),
        }
    }

    /// Consumes the `NodeArena`, returning the inner [Arena](::arena::Arena).
    ///
    /// # Panics
    ///
    /// Panics if the `NodeArena` has more than one strong reference.
    pub fn into_inner(self) -> Arena<NodeEntry<T>> {
        if let Ok(inner) = Arc::try_unwrap(self.inner) {
            inner.into_inner()
        } else {
            panic!("NodeArena::into_inner called while having more than one strong reference");
        }
    }

    /// Returns true if the `NodeArena` points to the same inner arena as the other one.
    pub fn ptr_eq(&self, other: &NodeArena<T>) -> bool {
        Arc::ptr_eq(&self.inner, &other.inner)
    }

    /// Returns a handle to inner [Arena](::arena::Arena).
    pub fn read<'a>(&'a self) -> impl Deref<Target = Arena<NodeEntry<T>>> + 'a {
        self.inner.read()
    }

    /// Returns a mutable handle to inner [Arena](::arena::Arena).
    pub fn write<'a>(&'a self) -> impl DerefMut<Target = Arena<NodeEntry<T>>> + 'a {
        self.inner.write()
    }

    pub(crate) fn read_guard<'a>(&'a self) -> RwLockReadGuard<'a, Arena<NodeEntry<T>>> {
        self.inner.read()
    }

    pub(crate) fn write_guard<'a>(&'a self) -> RwLockWriteGuard<'a, Arena<NodeEntry<T>>> {
        self.inner.write()
    }
}

impl<T> Clone for NodeArena<T> {
    fn clone(&self) -> NodeArena<T> {
        NodeArena {
            inner: self.inner.clone(),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn ptr_eq() {
        let arena = NodeArena::<i32>::new();
        let copy = arena.clone();

        assert!(arena.ptr_eq(&copy));
    }

    #[test]
    fn read() {
        let arena = NodeArena::<i32>::with_capacity(1024);
        assert_eq!(arena.read().capacity(), 1024);
    }

    #[test]
    fn into_inner() {
        let arena = NodeArena::<i32>::new();
        assert_eq!(arena.into_inner().capacity(), 0);
    }

    #[test]
    fn write() {
        let arena = NodeArena::<i32>::with_capacity(1024);
        arena.write().clear();
    }
}
