use std::iter::{ExactSizeIterator, FusedIterator};
use std::mem;
use std::num::NonZeroUsize;
use std::ops::{Index, IndexMut};

use fnv::FnvHashSet;
use onevec::OneVec;

/// A unique identifier associated with `Arena` element.
///
/// It is defined as a wrapper around NonZeroUsize, so size of
/// `Option<Id>` should be exactly the same as the size of `usize`.
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
pub struct Id(NonZeroUsize);

impl Id {
    fn inc(self) -> Id {
        Id(unsafe { NonZeroUsize::new_unchecked(self.0.get() + 1) })
    }

    fn dec(self) -> Id {
        Id(unsafe { NonZeroUsize::new_unchecked(self.0.get() - 1) })
    }
}

/// A map, providing fast indexing, insertion and removal of elements,
/// but not preserving element order.
///
/// Every element is associated with a unique [id](::arena::Id).
///
/// Inside, it uses a fast `FnvHashSet` with free ids.
///
/// Every time an element is removed, it's id is being written there,
/// so there is no need to shift other elements to the empty space.
#[derive(Debug, Clone)]
pub struct Arena<T> {
    elements: OneVec<T>,
    vacant: FnvHashSet<Id>,
}

impl<T> Arena<T> {
    /// Constructs an empty `Arena`.
    ///
    /// The arena won't allocate until elements are pushed onto it.
    pub fn new() -> Arena<T> {
        Arena {
            elements: OneVec::new(),
            vacant: FnvHashSet::default(),
        }
    }

    /// Constructs a new `Arena` with specified capacity. If capacity
    /// is `0`, the arena won't allocate.
    pub fn with_capacity(capacity: usize) -> Arena<T> {
        Arena {
            elements: OneVec::with_capacity(capacity),
            vacant: FnvHashSet::default(),
        }
    }

    /// Returns the capacity of `Arena`.
    pub fn capacity(&self) -> usize {
        self.elements.capacity()
    }

    /// Returns the number of elements in `Arena`.
    pub fn len(&self) -> usize {
        self.elements.len() - self.vacant.len()
    }

    /// Returns true if the arena contains no elements.
    pub fn is_empty(&self) -> bool {
        self.len() == 0
    }

    /// Inserts a new element into the arena and returns
    /// an id associated with it.
    pub fn insert(&mut self, element: T) -> Id {
        let mut iter = self.vacant.iter();

        if let Some(&id) = iter.next() {
            self.elements[id.0] = element;
            id
        } else {
            self.elements.push(element);
            Id(unsafe { NonZeroUsize::new_unchecked(self.elements.len()) })
        }
    }

    /// Returns true if the arena contains given id.
    pub fn contains(&self, id: Id) -> bool {
        !self.vacant.contains(&id) && id.0.get() <= self.elements.len()
    }

    /// Returns a reference to an element in the arena.
    ///
    /// If the specified id does not exists, returns `None`.
    pub fn get(&self, id: Id) -> Option<&T> {
        if self.contains(id) {
            Some(&self.elements[id.0])
        } else {
            None
        }
    }

    /// Returns a mutable reference to an element in the arena.
    ///
    /// If the specified id does not exists, returns `None`.
    pub fn get_mut(&mut self, id: Id) -> Option<&mut T> {
        if self.contains(id) {
            Some(&mut self.elements[id.0])
        } else {
            None
        }
    }

    /// Removes and returns the element with specified id.
    ///
    /// If the id does not exists, returns `None`.
    pub fn remove(&mut self, id: Id) -> Option<T> {
        if self.contains(id) {
            self.vacant.insert(id);
            let old = mem::replace(&mut self.elements[id.0], unsafe { mem::uninitialized() });
            Some(old)
        } else {
            None
        }
    }

    /// Clears the arena.
    ///
    /// This method does not change the capacity of the arena.
    pub fn clear(&mut self) {
        self.elements.clear();
        self.vacant.clear();
    }

    /// Returns an iterator over the arena.
    pub fn iter<'a>(&'a self) -> Iter<'a, T> {
        Iter {
            arena: &self,
            first: Id(unsafe { NonZeroUsize::new_unchecked(1) }),
            last: Id(unsafe { NonZeroUsize::new_unchecked(self.elements.len() + 1) }),
            len: self.len(),
        }
    }

    /// Returns a mutable iterator over the arena.
    pub fn iter_mut<'a>(&'a mut self) -> IterMut<'a, T> {
        let last = Id(unsafe { NonZeroUsize::new_unchecked(self.elements.len() + 1) });
        let len = self.len();

        IterMut {
            last,
            len,
            arena: self,
            first: Id(unsafe { NonZeroUsize::new_unchecked(1) }),
        }
    }
}

impl<T> Index<Id> for Arena<T> {
    type Output = T;

    fn index(&self, index: Id) -> &T {
        self.get(index).unwrap()
    }
}

impl<T> IndexMut<Id> for Arena<T> {
    fn index_mut(&mut self, index: Id) -> &mut T {
        self.get_mut(index).unwrap()
    }
}

/// An immutable iterator over the arena.
pub struct Iter<'a, T: 'a> {
    arena: &'a Arena<T>,
    first: Id,
    last: Id,
    len: usize,
}

impl<'a, T: 'a> Iterator for Iter<'a, T> {
    type Item = &'a T;

    fn next(&mut self) -> Option<Self::Item> {
        if self.first.0.get() >= self.last.0.get() {
            return None;
        }

        let mut cur = self.first;

        while self.arena.vacant.contains(&self.first) {
            cur = self.first.inc();
        }

        self.first = cur.inc();

        self.len -= 1;
        self.arena.get(cur)
    }

    fn size_hint(&self) -> (usize, Option<usize>) {
        (self.len, Some(self.len))
    }
}

impl<'a, T: 'a> DoubleEndedIterator for Iter<'a, T> {
    fn next_back(&mut self) -> Option<Self::Item> {
        if self.last.0.get() <= self.first.0.get() {
            return None;
        }

        while self.arena.vacant.contains(&self.last.dec()) {
            if let Some(id) = NonZeroUsize::new(self.last.0.get() - 1).map(|v| Id(v)) {
                self.last = id;
            } else {
                return None;
            }
        }

        self.last = self.last.dec();

        self.len -= 1;
        self.arena.get(self.last)
    }
}

impl<'a, T: 'a> FusedIterator for Iter<'a, T> {}
impl<'a, T: 'a> ExactSizeIterator for Iter<'a, T> {}

/// A mutable iterator over the arena.
pub struct IterMut<'a, T: 'a> {
    arena: &'a mut Arena<T>,
    first: Id,
    last: Id,
    len: usize,
}

impl<'a, T: 'a> Iterator for IterMut<'a, T> {
    type Item = &'a mut T;

    fn next(&mut self) -> Option<Self::Item> {
        if self.first.0.get() >= self.last.0.get() {
            return None;
        }

        let mut cur = self.first;

        while self.arena.vacant.contains(&self.first) {
            cur = self.first.inc();
        }

        self.first = cur.inc();

        self.len -= 1;
        self.arena
            .get_mut(cur)
            .map(|v| unsafe { &mut *(v as *mut T) })
    }

    fn size_hint(&self) -> (usize, Option<usize>) {
        (self.len, Some(self.len))
    }
}

impl<'a, T: 'a> DoubleEndedIterator for IterMut<'a, T> {
    fn next_back(&mut self) -> Option<Self::Item> {
        if self.last.0.get() <= self.first.0.get() {
            return None;
        }

        while self.arena.vacant.contains(&self.last.dec()) {
            if let Some(id) = NonZeroUsize::new(self.last.0.get() - 1).map(|v| Id(v)) {
                self.last = id;
            } else {
                return None;
            }
        }

        self.last = self.last.dec();

        self.len -= 1;
        self.arena
            .get_mut(self.last)
            .map(|v| unsafe { &mut *(v as *mut T) })
    }
}

impl<'a, T: 'a> FusedIterator for IterMut<'a, T> {}
impl<'a, T: 'a> ExactSizeIterator for IterMut<'a, T> {}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn create() {
        let a = Arena::<i32>::new();
        assert_eq!(a.capacity(), 0);

        let a = Arena::<i32>::with_capacity(10);
        assert_eq!(a.capacity(), 10);
        assert!(a.is_empty());
    }

    #[test]
    fn simple() {
        let mut a = Arena::new();
        let id = a.insert(10);

        assert!(a.contains(id));
        assert!(a.capacity() >= 1);
        assert_eq!(a.len(), 1);
        assert!(!a.is_empty());
        assert_eq!(*a.get(id).unwrap(), 10);

        *a.get_mut(id).unwrap() = 20;

        assert_eq!(a[id], 20);

        a[id] = 30;

        assert_eq!(a.remove(id).unwrap(), 30);
        assert!(a.remove(id).is_none());
        assert!(a.is_empty());
    }

    #[test]
    fn iter() {
        let mut a = Arena::new();
        a.insert(1);
        a.insert(2);
        a.insert(3);
        a.insert(4);

        let mut iter = a.iter();
        assert_eq!(iter.len(), 4);
        assert_eq!(*iter.next().unwrap(), 1);
        assert_eq!(iter.len(), 3);
        assert_eq!(*iter.next_back().unwrap(), 4);
        assert_eq!(iter.len(), 2);
        assert_eq!(*iter.next().unwrap(), 2);
        assert_eq!(iter.len(), 1);
        assert_eq!(*iter.next_back().unwrap(), 3);
        assert_eq!(iter.len(), 0);
        assert!(iter.next().is_none());
        assert!(iter.next().is_none());
        assert!(iter.next_back().is_none());
        assert!(iter.next_back().is_none());
    }

    #[test]
    fn iter_mut() {
        let mut a = Arena::new();
        a.insert(1);
        a.insert(2);
        a.insert(3);
        a.insert(4);

        let mut iter = a.iter_mut();
        assert_eq!(iter.len(), 4);
        assert_eq!(*iter.next().unwrap(), 1);
        assert_eq!(iter.len(), 3);
        assert_eq!(*iter.next_back().unwrap(), 4);
        assert_eq!(iter.len(), 2);
        assert_eq!(*iter.next().unwrap(), 2);
        assert_eq!(iter.len(), 1);
        assert_eq!(*iter.next_back().unwrap(), 3);
        assert_eq!(iter.len(), 0);
        assert!(iter.next().is_none());
        assert!(iter.next().is_none());
        assert!(iter.next_back().is_none());
        assert!(iter.next_back().is_none());
    }
}
