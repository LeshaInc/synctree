# SyncTree

A crate providing tree implementation with multithreading support.

```rust
use synctree::NodeArena;

let ref arena = NodeArena::with_capacity(12);
let root = tree!(
    arena,
    1 => [
        2,
        3 => [4, 5],
        6 => [
            7 => [8 => []],
            9 => [10 => [11]]],
        12
    ]
);
```

This code creates a following tree:

![graph](img/graph.png)
